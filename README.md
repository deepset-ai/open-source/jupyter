# Purpose
A jupyter docker that
-  runs locally or in the cloud
-  runs on gpu or cpu
-  includes DL frameworks (tensorflow and keras)
-  includes major python packages for daily work (numpy, pandas, spacy ...)
-  fosters collaboration by providing the same environment to all developers

# Implementation

We customize docker images from floydhub (https://github.com/floydhub/dockerfiles/tree/master/dl/tensorflow/1.7.0), who provide (and maintain :) ) images for many popular DL environments.

There are two docker images: one for CPU and one for GPU machines.

# Usage

## 1) Build docker
### CPU
`docker build -t registry.gitlab.com/deepset-ai/open-source/jupyter:cpu -f Dockerfile.cpu`
### GPU
`docker build -t registry.gitlab.com/deepset-ai/open-source/jupyter:gpu -f Dockerfile.gpu`
## 3) Run docker
replace the path of the mounted volume ($DEEPSET_HOME) with your own if necessary (these files are available in jupyter notebook).
### CPU

```
cd cpu-start  
docker-compose up -d
``` 


or

`docker run -it -p 8888:8888 -p 6006:6006 --name jupyter -v $DEEPSET_HOME:/root/sharedfolder registry.gitlab.com/deepset-ai/open-source/jupyter:cpu`
### GPU
```
cd cpu-start
docker-compose up -d
```

or

`nvidia-docker run -it -p 8888:8888 -p 6006:6006 --name jupyter-gpu -v $DEEPSET_HOME:/root/sharedfolder registry.gitlab.com/deepset-ai/open-source/jupyter:gpu`

## 4) Jupyter
Click on URL (incl. security token) from starting logs of docker container `docker logs jupyter`

# CI
The CI pipeline builds both new docker images and pushes them to the gitlab and dockerhub registry.
